Laboratori: M3 - UF1 – T2 – E6	

**UF1. Estructures seqüencials, alternatives i iteratives.**	


|Material de consulta:|<p>**Teoria:**</p><p>- UF1 - T1 - Estructura d'un programa informàtic.</p><p>- UF1 - T2 - Programació estructurada</p><p>**Annexos:**</p><p>- UF1 - T2 - Annex E6 (iteracions)</p><p>- UF1 - T2 - Sentències de trencament o salt en C.</p><p>**Explicacions de classe.**</p>|
| :- | :- |
|Format del lliurament:|<p>**nomUsuari\_UF1\_Be2\_E6**</p><p>Per exemple si l’usuari és *asanchez* el nom seria “*asanchez \_UF1\_Be2\_E6*”</p><p>Les respostes feu-les en color blau.</p>|
|Durada:|Establerta al portal web de l’assignatura.|

1. Estructures algorísmiques.

(***Els exercicis s’han de codificar en ANSI C**).*

**Nota:** utilitzeu les estructures iteratives més adequades en cada exercici.

Exercici 1:	 Justifica la utilització de les estructures iteratives que coneixes i posa’n exemples.

**while():** per fer el while necessites tenir les dades a les variables abans del while.

**Exemple while:**

Introdueix el pin del telèfon.

En aquest cas també es podria fer amb el do while pero com que el primer intent seria a fora del while (el d'introduir) llavors s’incialitzaria a 1 si s’ha de fer x vegades o fins ha encertar depenent de si hi ha màxim de intents o no.

**for:** S’utilitza per quan saps les n vegades a iterar.

**Exemple for:**

Introdueix 5 vegade la edad.

**do{ } while()** a diferencia del while no requereix tenir dades abans les pots agafar dins del do  i llavors cada iteració anirà revisant-se amb la condició  del while del final del do.

**Exemple  do{ } while() :**

Introdueix el pin del telèfon.

En aquest cas també es podria fer amb el while pero el primer intent seria a dins del do while llavors s’incialitzaria a 0 si s’ha de fer x vegades o fins ha encertar depenent de si hi ha màxim de intents o no.


Exercici 2:	 Disseny d’un algorisme que ens demani l’edat 100 vegades.

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int vegades=100;

    int i =0;

    int edat;

    while(vegades>=i){

        printf("Introdueix la teva edat\n");

        scanf("%d",&edat);

        i++;

    }

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.001.png)

Exercici 3:	 Disseny d’un algorisme que mostri els 100 primers números naturals.

#include <stdio.h>

#include <stdlib.h>

```
int main()

{

    int vegades=101;

    int i =1;

    while(vegades!=i){

        printf("%d,",i);

        i++;

    }

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.002.png)



Exercici 4:	 Disseny d’un algorisme que mostri els nombres parells de l’1 al 100.

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int vegades=101;

    int i =2;

    while(vegades>=i){

        printf("%d,",i);

        i=i+2;

    }

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.003.png)

Exercici 5:	 Disseny d’un algorisme que mostri els nombres senars de l’1 al 100.

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int vegades=100;

    int i =1;

    while(vegades>=i){

        printf("%d,",i);

        i=i+2;

    }

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.004.png)

Exercici 6:	 Disseny d’un algorisme que mostri els nombres primers que hi ha entre l’1 i el 100 inclosos.

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int j, i, a;

    for(j=2;j<=100;j++){

        a=0;

        for(i=1;i<=100;i++){

            if(j%i==0)

            a++;

         }

        if(a==2){

             printf("%d\n",j);

        }

    }

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.005.png)

Exercici 7:

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.006.png)

Disseny d’un algorisme que compti de 5 en 5 fins a 100.

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int i, max;

    i=5;

    max=100;

    while(max>=i){

        printf("%d ",i);

        i=i+5;

    }

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.007.png)

Exercici 8:

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.008.png)

Disseny d’un algorisme que vagi del número 100 al número 1 de forma Descendent.

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int i, min;

    i=100;

    min=1;

    while(min<=i){

        printf("%d ",i);

        i--;

    }

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.009.png)


Exercici 9:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.010.png)

Disseny d’un algorisme que mostri “*n*” termes de la successió de Fibonacci. Els dos primers termes valen 1 i el terme n-èssim és la suma dels dos anteriors. És a dir, *an = an-1 + an-2* .

```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int n1, n2, n3,n4,aux ;

    n1=1;

    n2=1;

    n4=1;

    printf("Introdueix el nombre de numeros a treure de la succeció de fibonacci\n");

    scanf("%d",&n3);

    if(n3==1){

        printf("%d",n1);

    }

    else if(n3==2){

        printf("%d, %d",n1,n2);

    }else if(n3>2){

        while(n3>=n4){

            aux=n1;

            n1=n2;

            n2=n1+aux;

            printf("%d, ",aux);

            n4++;

        }

    }

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.011.png)

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.012.png)

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.013.png)

Exercici 10:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.014.png)

A partir de la següent especificació: { anys=ANYS ^  dies=DIES ^

^hores=HORES ^ minuts= MINUTS ^ segons=SEGONS ^ anys>0 ^

^0<=dies<365 ^ 0<=hores<24 ^0<=minuts<60 ^ 0<=segons<60 }. 

Dissenyeu un algorisme que incrementi el temps en un segon.



```
#include <stdio.h>

#include <stdlib.h>

int main()

{

    int anys2, dies2, hores2, minuts2, segons2;

    anys2=0;

    dies2=1;
    
    hores2=0;

    minuts2=0;

    segons2=0;

    while(anys2!=1){

        if(segons2!=60){

            segons2++;

         printf("Any: %d, Dies: %d, Hores: %d, Minuts: %d, Segons: %d\n",anys2,dies2,hores2,minuts2,segons2);

        }else if(segons2==60){

            minuts2++;

            segons2=0;

            printf("Any: %d, Dies: %d, Hores: %d, Minuts: %d, Segons: %d\n",anys2,dies2,hores2,minuts2,segons2);

        }

        if(minuts2==60){

            hores2++;

            minuts2=0;

            printf("Any: %d, Dies: %d, Hores: %d, Minuts: %d, Segons: %d\n",anys2,dies2,hores2,minuts2,segons2);

        }if(hores2==24){

            dies2++;

            hores2=0;

            printf("Any: %d, Dies: %d, Hores: %d, Minuts: %d, Segons: %d\n",anys2,dies2,hores2,minuts2,segons2);

        }if(dies2==365){

            anys2++;

            dies2=1;

            printf("Any: %d, Dies: %d, Hores: %d, Minuts: %d, Segons: %d\n",anys2,dies2,hores2,minuts2,segons2);

        }

    }

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.015.png)


Exercici 11:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.016.png)

Disseny d’un algorisme que calculi les solucions d’una equació de 2n. grau.

```
#include <stdio.h>

#include <stdlib.h>

#include <math.h>

int main()

{

    double a,b,c,radicant,x1,x2;

    a=5;

    b=150;

    c=4;

    radicant=pow(b, 2.0)-4\*a\*c;

    if(radicant>0.0){

        x1=((-b+sqrt(radicant))/(2.0\*a));

        x2=((-b-sqrt(radicant))/(2.0\*a));

        printf("\nLa solucio de x1=%.4f \nLa solucio de x2=%.4f", x1, x2);

    }

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.017.png)

Exercici 12:

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.018.png)

Donat un text calcular la seva longitud. Recorda que un text, en C, acaba amb el caràcter de final de cadena ‘\0’. A partir d’ara “text” i/o “paraula” ho llegirem com cadena de caràcters o *string*.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

int main()

{
    char nom[MAX\_TEXT+1];

    int comptador;

    printf("Introdueix el text: ");

    scanf("%50[^\n]",nom);

    comptador=0;

    while(nom[comptador]!='\0') comptador++;

    printf("El teu text te %i caracters",comptador);

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.019.png)

Exercici 13:

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.020.png)

Donat un text comptar el nombre de vocals.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

int main()

{

    char text[MAX\_TEXT+1];

    int comptador, comptadorVocals;

    printf("Introdueix el teu nom: ");

    scanf("%100[^\n]",text);

    comptador=0;

    comptadorVocals=0;

    while(text[comptador]!='\0'){

        if(text[comptador]=='a'||

        text[comptador]=='e'||

        text[comptador]=='i'||

        text[comptador]=='o'||

        text[comptador]=='u'||

        text[comptador]=='A'||

        text[comptador]=='E'||

        text[comptador]=='I'||

        text[comptador]=='O'||

        text[comptador]=='U')comptadorVocals++;

        comptador++;

    }

    printf("El numero de vocals es: %d",comptadorVocals);


    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.021.png)

Exercici 14:

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.022.png)

Donat un text comptar el nombre de consonants.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

int main()

{

    char text[MAX\_TEXT+1];

    int comptador, comptadorConsonats;

    printf("Introdueix el teu nom: ");

    scanf("%100[^\n]",text);

    comptador=0;

    comptadorConsonats=0;

    while(text[comptador]!='\0'){

        if(!(text[comptador]=='a'||

        text[comptador]=='e'||

        text[comptador]=='i'||

        text[comptador]=='o'||

        text[comptador]=='u'||

        text[comptador]=='A'||

        text[comptador]=='E'||

        text[comptador]=='I'||

        text[comptador]=='O'||

    text[comptador]=='U'))comptadorConsonats++;

    comptador++;

    }

    printf("El numero de consonants es: %d",comptadorConsonats);


    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.023.png)

Exercici 15:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.024.png)

Donat un text capgirar-lo.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

int main()

{

    char text[MAX\_TEXT+1];

    int index;

    printf("Introdueix el text a capgirar: ");

    scanf("%100[^\n]",text);

    index=0;

    while(text[index]!='\0'){

        index++;

    }

    while(index>0){

        index--;

        printf("%c",text[index]);

    }

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.025.png)




Exercici 16:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.026.png)

Donat un text comptar el nombre de paraules que acaben en “*ts*”.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

int main()

{

    char text[MAX\_TEXT+1];

    int index;

    int comptador\_ts;

    int comptador\_lletra;

    index=0;

    comptador\_ts=0;

    comptador\_lletra=0;

    printf("Comptar les paraules que acaben en ts!\n");

    printf("Introdueix un text: ");

    scanf("%100[^\n]",text);

    while(text[comptador\_lletra]!='\0') comptador\_lletra++;

        if(comptador\_lletra>=2){

            while(text[index]!='\0'){

                while(text[index]==' ')index++;

                while(text[index]!=' ' &&

                text[index]!='\0')index++;

                if(text[index-1]=='s' &&

                text[index-2]=='t' ||

                text[index-1]=='S' &&

                text[index-2]=='T' ||

                text[index-1]=='s' &&

                text[index-2]=='T' ||

                text[index-1]=='S' &&

                text[index-2]=='t')comptador\_ts++;

             }

            printf("\nHi ha %d paraules que acaben en ts.\n",comptador\_ts);

        }

    else printf("\nNo hi han paraules acabades amb ts?\n");

    return 0;

}


```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.027.png)

Exercici 17:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.028.png)

Donat un text comptar el nombre de paraules.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

#define MAX\_PAR 10

#define MAX\_CAR 10

int main(){

    char text[MAX\_TEXT+1];

    char llistaPar[MAX\_PAR][MAX\_CAR+1];

    int it, ip, ic;

    text[0]='\0';

    it=0;

    ip=0;

    while(it<MAX\_PAR){

        llistaPar[it][0]='\0';

        it++;

    }

    it=0;

    printf("Introdueix un text: ");

    scanf("%100[^\n]",text);

    while(text[it]!='\0'){

        //saltar blancs

        while(text[it]==' ')it++;

        //obtenir paraula

        ic=0;

             while(text[it]!=' ' &&

            text[it]!='\0'){

                llistaPar[ip][ic]=text[it];

                it++;

                ic++;
            }

            llistaPar[ip][ic]='\0';

            //següent paraula de la llista

            ip++;

    }
    printf("Text: %s",text);

    it=0;

    while(it<ip){

        printf("\nLa paraula %d es: %s",it+1,llistaPar[it]);

        it++;

    }
    printf("\nHi ha %d paraula/es",ip);

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.029.png)


Exercici 18:
![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.030.png)

Donat un text dissenyeu un algorisme que compti els cops de apareixen conjuntament la parella de caràcters “*as*” dins del text.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX\_TEXT 100

int main(){

    char text[MAX\_TEXT+1];

    int i,as;

    i=0;

    as=0;

    printf("Introdueix el text: ");

    scanf("%100[^\n]",text);

    while(text[i]!='\0'){

        while(text[i]==' ')i++;

        while(text[i]!=' ' && text[i]!='\0'){

            if(text[i]=='a' && text[i+1]=='s'){

                as++;

            }

            i++;

        }

    }

    printf("Hi han %d as.",as);

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.031.png)



Exercici 19:	                                                                                         Donat un text i una paraula (anomenada *parbus*). Dissenyeu un algorisme que comprovi si la paraula és troba dins del text.

**Nota**: *parbus* és el nom de la variable que conté la paraula a cercar.

```
#include <stdio.h>

#include <stdlib.h>

#include <stdbool.h>

#define bb while(getchar()!='\n');

#define MAX\_TEXT 100

#define MAX\_PARBUS 100

#define MAX\_PAR 100

int main()

{

    char text[MAX\_TEXT+1];

    char parbus[MAX\_PAR+1];

    char partmp[MAX\_PARBUS+1];

    int it, ip, itmp;

    bool trobada=false;

    it=0;

    ip=0;

    itmp=0;

    text[0]='\0';

    parbus[0]='\0';

    partmp[0]='\0';

    printf("Escriu un text: ");

    scanf("%100[^\n]",text);

    bb;

    printf("Escriu la parbus: ");

    scanf("%100[^\n]",parbus);

    while(text[it]!='\0'){              //fins al final

        while(text[it]==' ')it++;       //saltar blancs

        itmp=0;

        while(text[it]!=' ' &&          //copiar paraula

        text[it]!='\0'){                  
            partmp[itmp]=text[it];

            itmp++;

            it++;

        }

        partmp[itmp]='\0';

        while(partmp[ip]==parbus[ip] &&

        (partmp[ip]!='\0' || parbus[ip]!='\0'))ip++;

        if(partmp[ip]==parbus[ip]){

        printf("\nTrobada! \*%s\* esta al text.",parbus);

        trobada=true;

        break;
        }

    }

    if(!trobada)printf("No l'he trobat.");

    return 0;

}
```


![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.032.png)


Exercici 20:	                                                                                         Donat un text i una paraula (*parbus*). Dissenyeu un algorisme que digui quants cops apareix la paraula (*parbus*) dins del text

```
#include <stdio.h>

#include <stdlib.h>

#include <stdbool.h>

#define bb while(getchar()!='\n');

#define MAX_TEXT 100

#define MAX_PARBUS 100

#define MAX_PAR 100

int main()

{

    char text[MAX_TEXT+1];

    char parbus[MAX_PAR+1];

    char partmp[MAX_PARBUS+1];

    int it, ip, itmp, comptador;

    bool trobada=false;

    comptador=0;

    it=0;

    ip=0;

    text[0]='\0';

    parbus[0]='\0';

    partmp[0]='\0';

    printf("Escriu un text: ");

    scanf("%100[^\n]",text);

    bb;

    printf("Escriu la parbus: ");

    scanf("%100[^\n]",parbus);

    bb;

    while(text[it]!='\0'){              //fins al final

        while(text[it]==' ')it++;       //saltar blancs

        itmp=0;

        //copiar paraula
        while(text[it]!=' ' &&  text[it]!='\0'){

            partmp[itmp]=text[it];

            itmp++;

            it++;

        }

        partmp[itmp]='\0';

        ip=0;

        while(partmp[ip]==parbus[ip] &&

        (partmp[ip]!='\0' || parbus[ip]!='\0'))ip++;

        if(partmp[ip]==parbus[ip]){

        trobada=true;

        comptador++;

        }

    }

    if(!trobada)printf("No s'ha trobat.");

        if(trobada==true){printf("\nTrobada! ''%s'' esta al text %d vegades",parbus,comptador);

    }

    return 0;

}
```

![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.033.png)


Exercici 21:	                                                                                         Donat un text i una paraula (*parbus*) i una paraula (*parsub*). Dissenyeu un algorisme que substitueixi, en el text, totes les vegades que apareix la paraula (*parbus*) per la paraula (*parsub*).

**Nota**: *parbus* és el nom de la variable que conté la paraula a buscar i

*parsub* el nom de la variable que conté la paraula que volem substituir.

**Nota:** afegiu captures de pantalla del resultat d’executar els diferents exercicis que demostrin la seva correctesa.

```
#include <stdio.h>

#include <stdlib.h>

#define MAX_TEXT 100

#define MAX_TEXT2 100

#define MAX_PARBUS 100

#define MAX_PARSUB 100

#define MAX_PARTMP 100

#define BB while(getchar()!='\n')

int main()

{

    char text[MAX_TEXT+1];

    char text2[MAX_TEXT2+1];

    char parbus[MAX_PARBUS+1];

    char parsub[MAX_PARSUB+1];

    char partmp[MAX_PARTMP+1];

    int it, it2, ipb, ips, ipt;

    it=0;

    text[0]='\0';

    text2[0]='\0';

    parbus[0]='\0';

    parsub[0]='\0';

    partmp[0]='\0';

    printf("Introdueix el text: ");

    scanf("%100[^\n]",text);

    BB;

    printf("Introdueix la paraula a cercar: ");

    scanf("%100[^\n]",parbus);

    BB;

    printf("Introdueix la paraula a substituir: ");

    scanf("%100[^\n]",parsub);

    BB;

    while(text[it]!='\0'){

        //saltar blancs

        while(text[it]==' '){

            text2[it2]=text[it];

            it++;it2++;

        }

        //obtenir paraula

        ipt=0;

        while(text[it]!=' ' && text[it]!='\0'){

            partmp[ipt]=text[it];

            it++;ipt++;

        }

        partmp[ipt]='\0';

        //iguals partmp i parbus

        ipt=0;

        while(partmp[ipt]==parbus[ipt] && (partmp[ipt]!='\0' || parbus[ipt]!='\0'))

        ipt++;

        ipt=0;

        if(partmp[ipt]==parbus[ipt]){

             //copiar parsub text2

            while(parsub[ipt]!='\0'){

                text2[it2]=parsub[ipt];

                it2++;ipt++;

            }

        }

        else{

            //copiar partmp text2

            while(partmp[ipt]!='\0'){

                text2[it2]=partmp[ipt];

                it2++;ipt++;

            }

        }

    }

    text2[it2]='\0';

    printf("Text inicial: %s\n",text);

    printf("Text final: %s",text2);

    return 0;

}
```



![](Aspose.Words.e823a46b-8388-481f-b77f-f7db850b990e.034.png)

